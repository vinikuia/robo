class EmpresaBr:
    def __init__(self):
        setattr(self, 'cnpj', {'cnpjbiz': 'CNPJ', 'cnpjRocks': 'CNPJ', 'cnpjReceita': 'CNPJ',
                               'consultasCnpj': 'CNPJ', 'empresasCnpj': 'CNPJ'})
        setattr(self, 'id_matriz_filial', {'cnpjbiz': 'Tipo', 'cnpjRocks': 'Tipo', 'cnpjReceita': None,
                                           'consultasCnpj': 'Tipo', 'empresasCnpj': None})
        setattr(self, 'nome_empresarial',
                {'cnpjbiz': 'Razão Social', 'cnpjRocks': 'Razão Social', 'cnpjReceita': 'Razão Social',
                 'consultasCnpj': 'Nome', 'empresasCnpj': 'Razão social'})
        setattr(self, 'nome_fantasia',
                {'cnpjbiz': 'Nome Fantasia', 'cnpjRocks': 'Nome Fantasia', 'cnpjReceita': 'Nome fantasia',
                 'consultasCnpj': 'Fantasia', 'empresasCnpj': 'Nome fantasia'})
        setattr(self, 'situacao_cadastral',
                {'cnpjbiz': 'Situação', 'cnpjRocks': 'Situação', 'cnpjReceita': 'Situação atual',
                 'consultasCnpj': 'Situação', 'empresasCnpj': 'Status da empresa'})
        setattr(self, 'data_situacao_cadastral', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                                  'consultasCnpj': 'Data Situação', 'empresasCnpj': None})
        setattr(self, 'motivo_situacao_cadastral', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                                    'consultasCnpj': 'Motivo Situação', 'empresasCnpj': None})
        setattr(self, 'nome_cidade_exterior', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                               'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'codigo_pais', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                      'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'nome_pais', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                    'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'codigo_natureza_juridica',
                {'cnpjbiz': 'Natureza Jurídica', 'cnpjRocks': 'Natureza Jurídica', 'cnpjReceita': 'Natureza jurídica',
                 'consultasCnpj': 'Natureza Jurídica', 'empresasCnpj': ' Natureza jurídica'})
        setattr(self, 'data_inicio_atividade',
                {'cnpjbiz': 'Data da Abertura', 'cnpjRocks': 'Data da Abertura', 'cnpjReceita': 'Aberta em',
                 'consultasCnpj': 'Abertura', 'empresasCnpj': 'Data de abertura'})
        setattr(self, 'cnae_principal',
                {'cnpjbiz': None, 'cnpjRocks': 'Atividade Principal', 'cnpjReceita': 'Atividade econômica principal ',
                 'consultasCnpj': 'Atividade Principal ', 'empresasCnpj': 'Atividade econômica principal'})
        setattr(self, 'tipo_logradouro', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                          'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'logradouro', {'cnpjbiz': 'Logradouro', 'cnpjRocks': 'Logradouro', 'cnpjReceita': 'Logradouro',
                                     'consultasCnpj': 'Logradouro', 'empresasCnpj': 'Logradouro'})
        setattr(self, 'numero', {'cnpjbiz': None, 'cnpjRocks': 'Número', 'cnpjReceita': 'Número',
                                 'consultasCnpj': 'Número', 'empresasCnpj': 'Número'})
        setattr(self, 'complemento',
                {'cnpjbiz': 'Complemento', 'cnpjRocks': 'Complemento', 'cnpjReceita': 'Complemento',
                 'consultasCnpj': 'Complemento', 'empresasCnpj': 'Complemento'})
        setattr(self, 'bairro', {'cnpjbiz': 'Bairro', 'cnpjRocks': 'Bairro', 'cnpjReceita': 'Bairro',
                                 'consultasCnpj': 'Bairro', 'empresasCnpj': 'Bairro'})
        setattr(self, 'cep', {'cnpjbiz': 'CEP', 'cnpjRocks': 'CEP', 'cnpjReceita': 'CEP',
                              'consultasCnpj': 'CEP', 'empresasCnpj': 'CEP'})
        setattr(self, 'uf', {'cnpjbiz': 'Estado', 'cnpjRocks': 'UF', 'cnpjReceita': 'UF',
                             'consultasCnpj': 'UF', 'empresasCnpj': 'UF'})
        setattr(self, 'codigo_tom', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                     'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'municipio', {'cnpjbiz': 'Município', 'cnpjRocks': 'Município', 'cnpjReceita': 'Município',
                                    'consultasCnpj': 'Município', 'empresasCnpj': 'Município'})
        setattr(self, 'telefones', {'cnpjbiz': 'Telefone', 'cnpjRocks': 'Telefone', 'cnpjReceita': 'Telefone',
                                    'consultasCnpj': 'Telefone', 'empresasCnpj': 'Telefone'})
        setattr(self, 'endereco_e_mail', {'cnpjbiz': 'E-mail', 'cnpjRocks': 'E-mail', 'cnpjReceita': 'E-mail',
                                          'consultasCnpj': 'E-mail', 'empresasCnpj': 'Email'})
        setattr(self, 'qualificacao_responsavel',
                {'cnpjbiz': 'Qualificação', 'cnpjRocks': 'Qualificação', 'cnpjReceita': 'Qualificação ',
                 'consultasCnpj': 'Qualificação ', 'empresasCnpj': None})
        setattr(self, 'capital_social',
                {'cnpjbiz': 'Capital Social', 'cnpjRocks': 'Capital Social', 'cnpjReceita': 'Capital social',
                 'consultasCnpj': 'Capital Social', 'empresasCnpj': 'Capital Social'})
        setattr(self, 'porte_empresa', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                        'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'opcao_simples_nacional', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                                 'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'data_opcao_simples_nacional', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                                      'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'data_exclusao_simples_nacional', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                                         'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'opcao_mei', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                    'consultasCnpj': None, 'empresasCnpj': None})
        setattr(self, 'situacao_especial', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                            'consultasCnpj': 'Situação Especial', 'empresasCnpj': None})
        setattr(self, 'data_situacao_especial', {'cnpjbiz': None, 'cnpjRocks': None, 'cnpjReceita': None,
                                                 'consultasCnpj': 'Data Situação Especial', 'empresasCnpj': None})

# class EmpresaBr:
#     def __init__(self, **kwargs):
#         if not kwargs:
#             # for key in kwargs.keys():
#             cnpjbiz = kwargs.get('cnpjbiz')
#             cnpjrocks = kwargs.get('cnpjrocks')
#             cnpjreceita = kwargs.get('cnpjreceita')
#             consultascnpj = kwargs.get('consultascnpj')
#             empresascnpj = kwargs.get('empresascnpj')
#             normal = kwargs.get('normal')
#             if cnpjbiz:
#                 setattr(self, 'CNPJ', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Razão Social:', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Data da Abertura:', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Tipo', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Situação', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Natureza Jurídica', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'E-mail', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Telefone', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Logradouro', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Complemento', {'cn pjbiz': {'' : None}})
#                 setattr(self, 'Bairro', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'CEP', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Município', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'Estado', {'cnpjbiz': {'' : None}})
#             elif cnpjrocks:
#                 pass
#             elif cnpjreceita:
#                 pass
#             elif consultascnpj:
#                 pass
#             elif empresascnpj:
#                 pass
#             elif normal:
#                 setattr(self, 'cnpj',{'cnpjbiz': {'' : None}})
#                 setattr(self, 'id_matriz_filial', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'nome_empresarial', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'nome_fantasia', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'situacao_cadastral', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'data_situacao_cadastral', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'motivo_situacao_cadastral', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'nome_cidade_exterior', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'codigo_pais', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'nome_pais', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'codigo_natureza_juridica', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'data_inicio_atividade', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'cnae_principal', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'tipo_logradouro', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'logradouro', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'numero', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'complemento', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'bairro', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'cep', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'uf', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'codigo_tom', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'municipio', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'telefone1', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'telefone2', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'fax', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'endereco_e_mail', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'qualificacao_responsavel', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'capital_social', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'porte_empresa', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'opcao_simples_nacional', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'data_opcao_simples_nacional', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'data_exclusao_simples_nacional', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'opcao_mei', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'situacao_especial', {'cnpjbiz': {'' : None}})
#                 setattr(self, 'data_situacao_especial', {'cnpjbiz': {'' : None}})
# antigo
