class CnpjRocks:
    def __init__(self):
        setattr(self, 'EmpresaBr', {'CNPJ': None, 'Razão Social': None,
                                    'Data da Abertura': None,
                                    'Tipo': None,
                                    'Situação': None,
                                    'Natureza Jurídica': None,
                                    'Capital Social': None,
                                    'E-mail': None,
                                    'Nome Fantasia': None,
                                    'Telefone': [],
                                    'Logradouro': None,
                                    'Número': None,
                                    'Complemento': None,
                                    'Bairro': None,
                                    'CEP': None,
                                    'Município': None,
                                    'UF': None})
        setattr(self, 'Cnaes', {'CNPJ': None, 'Atividade Principal': None, 'Atividade Secundária': []})
        setattr(self, 'Sócio', {'CNPJ': None, 'Qualificação': [], 'nome': []})
#POSSUI CAPITAL SOCIAL