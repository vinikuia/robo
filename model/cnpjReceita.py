class CnpjReceita:
    def __init__(self):
        setattr(self, 'EmpresaBr', {'CNPJ': None, 'Razão Social ': None,
                                    'Aberta em': None,
                                    'Nome fantasia': None,
                                    'Situação atual': None,
                                    'Natureza jurídica': None,
                                    'Capital social': None,
                                    'E-mail': None,
                                    'Telefone': [],
                                    'Endereço': None,
                                    'Logradouro': None,
                                    'Número': None,
                                    'Complemento': None,
                                    'Bairro': None,
                                    'CEP': None,
                                    'Município': None,
                                    'UF': None})
        setattr(self, 'Cnaes', {'CNPJ': None, 'Atividade econômica principal ': None, 'Atividades econômicas '
                                                                                      'secundárias': []})
        setattr(self, 'Sócio', {'CNPJ': None, 'Qualificação ': [], 'Nome ': []})
        # DIFERENCIAL, HÁ COMO NAVEGAR NA PÁGINA PARA DESCOBRIR QUAIS OUTRAS EMPRESAS OS SÓCIOS PARTICIPAM

