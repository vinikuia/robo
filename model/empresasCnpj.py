class EmpresasCnpj:
    def __init__(self):
        setattr(self, 'EmpresaBr', {'CNPJ': None, 'Razão social': None,
                                    'Data de abertura': None,
                                    'Nome fantasia': None,  # default (ausente)
                                    'Status da empresa': None,
                                    'Natureza jurídica': None,
                                    'Capital Social': None,
                                    'Última Atualização': None,
                                    'Email': None,
                                    'Endereço': None,
                                    'Telefone': [],
                                    'Logradouro': None,
                                    'Número': None,
                                    'Complemento': None,
                                    'Bairro': None,
                                    'CEP': None,
                                    'Município': None,
                                    'UF': None})
        setattr(self, 'Cnaes', {'CNPJ': None, 'Atividade econômica principal': None})
        setattr(self, 'Sócio', {'CNPJ': None, 'Nome ': []})
        # DIFERENCIAL, HÁ COMO NAVEGAR NA PÁGINA PARA DESCOBRIR QUAIS OUTRAS EMPRESAS OS SÓCIOS PARTICIPAM
