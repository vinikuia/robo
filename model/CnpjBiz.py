class CnpjBiz:
    def __init__(self):
        setattr(self, 'EmpresaBr', {'CNPJ': None, 'Razão Social': None,
                                    'Data da Abertura': None,
                                    'Tipo': None,
                                    'Nome Fantasia': None,
                                    'Situação': None,
                                    'Capital Social': None,
                                    'Natureza Jurídica': None,
                                    'E-mail': None,
                                    'Telefone': [],
                                    'Logradouro': None,
                                    'Complemento': None,
                                    'Bairro': None,
                                    'CEP': None,
                                    'Município': None,
                                    'Estado': None})
        setattr(self, 'Cnaes', {'CNPJ': None, 'Principal': None, 'Secundária(s)': []})
        setattr(self, 'Sócios', {'CNPJ': None, 'tipo': [], 'nome': []})

# DIFERENCIAL: POSSUI NOME FANTASIA ALGUMAS
# VEZES E TEM EXPLICAÇÕES DE CADA TIPO DE CNAE, CASO ISSO QUEIRA SER UTILIZADO EM OUTRO MOMENTO
