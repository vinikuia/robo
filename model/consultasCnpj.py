class ConsultasCnpj:
    def __init__(self):
        setattr(self, 'EmpresaBr', {'CNPJ': None, 'Nome': None,
                                    'Abertura': None,
                                    'Fantasia': None,
                                    'Situação': None,
                                    'Motivo Situação': None,
                                    'Data Situação': None,
                                    'Situação Especial': None,
                                    'Data Situação Especial': None,
                                    'Natureza Jurídica': None,
                                    'Capital Social': None,
                                    'Última Atualização': None,
                                    'Tipo': None,
                                    'E-mail': None,
                                    'Telefone': [],
                                    'Logradouro': None,
                                    'Número': None,
                                    'Complemento': None,
                                    'Bairro': None,
                                    'CEP': None,
                                    'Município': None,
                                    'UF': None})
        setattr(self, 'Cnaes', {'CNPJ': None, 'Atividade Principal ': None, 'Atividades Secundárias': []})
        setattr(self, 'Sócio', {'CNPJ': None, 'Qualificação ': [], 'Nome ': []})
        # DIFERENCIAL, HÁ COMO NAVEGAR NA PÁGINA PARA DESCOBRIR QUAIS OUTRAS EMPRESAS OS SÓCIOS PARTICIPAM
        # HÁ DATA DA ÚLTIMA ATUALIZAÇÃO TAMBÉM
