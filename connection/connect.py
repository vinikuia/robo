import psycopg2
from time import sleep


class DBManager:
    def __init__(self, **kwargs):
        """
        Cria um objeto DBMan para gerenciamento do acesso ao banco de dados
        :param kwargs: dicionario com os parametros, valores possiveis:
        user = Usuario, padrao = root;
        senha = Senha correspondente ao usuario, padrao = '';
        end = endereço do host servidor do banco de dados, padrão = 'localhost';
        port = porta para a conexão ao banco de dados, padrao = 3306;
        database = Nome do banco de dados a ser acessado, padrão = 'mysql'.
        """
        self._cfg = kwargs
        self.conn = None

    def abrir_con(self):
        """
        Abre a conexão utilizando os parametros enviados no construtor
        :return: True se a conexão for aberta.
        """
        i = 30
        j = 0
        try:
            con = psycopg2.connect(**self._cfg)
            con.autocommit = False
            self.conn = con
            return True
        except Exception as e:
            raise e

    def fechar_con(self):
        """
        Fecha a conexão aberta e realiza o commit ou rollback das transações
        :param commit: indica se deve realizar o commit ou o rollback.
        :return: None
        """
        try:
            self.conn.close()
        except Exception as e:
            print(e)

    def executar_inup(self, sql: str, valores: tuple) -> int:
        """
        ATENÇÂO!! ESTE METODO NAO ABRE E NEM REALIZA O COMMIT DA TRANSAÇÃO
        Executa um comando do tipo insert ou update na conexão ativa
        :param sql: String sql a ser executada
        :param valores: valores que subistituiram os %s na string sql.
        :return: numero de linhas afetadas.
        """
        cursor = self.conn.cursor()
        cursor.execute(sql, valores)
        cursor.close()
        return cursor.rowcount

    # def executar_select(self, sql: str, valores: list = None) -> list:
    #     """
    #     ATENÇÂO!! ESTE METODO NAO ABRE A CONEXÃO E NEM REALIZA O COMMIT DA TRANSAÇÃO
    #     Executa um comando do tipo select na conexão ativa
    #     :param sql: String sql a ser executada
    #     :param valores: valores que subistituiram os %s na string sql.
    #     :return: lista com os valores retornados pelo select
    #     """
    #     try:
    #         cursor = self.conn.cursor()
    #         cursor.execute(sql, valores)
    #         return cursor.fetchall()
    #     except Exception as e:
    #         self.fechar_con()
    #         raise Exception('Erro ao executar sql Causa:', e)

    def executar_select(self, sql) -> list:
        """
        ATENÇÂO!! ESTE METODO NAO ABRE A CONEXÃO E NEM REALIZA O COMMIT DA TRANSAÇÃO
        Executa um comando do tipo select na conexão ativa
        :param sql: String sql a ser executada
        :param valores: valores que subistituiram os %s na string sql.
        :return: lista com os valores retornados pelo select
        """
        try:

            cursor = self.conn.cursor()
            cursor.execute(sql)
            return cursor.fetchall()
        except Exception as e:
            self.fechar_con()
            raise Exception('Erro ao executar sql Causa:', e)
