# from cryptography.fernet import Fernet
#
# key = Fernet.generate_key()
# print(key)
# file = open ('key.key','wb')
# file.write(key)
# file.close()
# import base64
# import os
# from cryptography.hazmat.backends import default_backend
# from cryptography.hazmat.primitives import hashes
# from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC
#
# password_provided = "secret_password" #input in a string form
# password=password_provided.encode()#convert string to bytes
# salt = os.urandom(16)
# kdf = PBKDF2HMAC(
#     algorithm=hashes.SHA256(),
#     length=32,
#     salt=salt,
#     iterations=100000,
#     backend=default_backend()
# )
# key = base64.urlsafe_b64decode(kdf.derive(password))
# print(key)
# from cryptography.fernet import Fernet
# #open file and read key
# file = open('key.key','rb')
# key=file.read()
# file.close()
# #encode a message, encode simply transform a messa to bytes making it public avaiable for everything
# message = '10.250.250.248'
# encoded=message.encode()
# #now this is getting encryped
# f= Fernet(key)
# encrypted = f.encrypt(encoded)
# #descrypt
# f= Fernet(key)
# decrypted = f.decrypt(encrypted)
# print(decrypted)
# #it is still a bytes message till now
# #let's make it like the original message now
# original_message =decrypted.decode()
# print(original_message)
# #create an archive.cfg
# # file = open ('key.key','rb')
# # key =  file.read()
# # file.close()
# # file = open ('Nsinova.cfg','w')
# # seq = ['[DataBase]\n','host="10.250.250.248"\n', 'port="5432"\n', 'dbname="empresas_brasileiras"\n',
# #                         'user="postgres"\n', 'password="pgsql$nsinova"\n']
# # file.writelines(seq)
from cripto.crypto import Cripto
from configparser import ConfigParser
from os.path import abspath

class KeyWork:
    def __init__(self, chave: str = None, arquivo: str = None):
        """
        abre um objeto gerenciador de arquivos de configuração com criptogradia
        :param chave: chave do arquivo a ser aberto
        :param arquivo: diretorio do arquivo
        """
        self.cfg = ConfigParser()
        if chave is not None:
            self.cripto = Cripto(bytes(chave))
            self.dir_arquivo = arquivo
            try:
                self.cfg.read(self.dir_arquivo)
            except Exception as e:
                raise Exception('Não foi possivel carregar o arquivo ', abspath(arquivo), 'Causa:', e)
        # self.set_db_name('empresas_brasileiras')
        # self.set_db_passwd('pgsql$nsinova')
        # self.set_db_port(5432)
        # self.set_db_host('10.250.250.248')
        # self.set_db_user('postgres')

    def open(self, diretorio: str):
        self.cfg.read(diretorio)

    def save(self, diretorio: str = '../proj.cfg'):
        try:
            file = open(diretorio, 'x')
        except FileExistsError:
            file = open(diretorio, 'w')
        finally:
            self.cfg.write(file)
            file.close()


    def set_db_user(self, value: str):
        user = self.cripto.criptografar(value)
        try:
            self.cfg['DATABASE']['User'] = user
        except KeyError:
            self.cfg.add_section('DATABASE')
            self.cfg.set('DATABASE', 'User', user)


    def get_db_user(self):
        user = self.cfg['DATABASE']['User']
        return self.cripto.decriptografar(user)


    def set_db_passwd(self, value: str):
        passwd = self.cripto.criptografar(value)
        try:
            self.cfg['DATABASE']['Password'] = passwd
        except KeyError:
            self.cfg.add_section('DATABASE')
            self.cfg.set('DATABASE', 'Password', passwd)


    def get_db_passwd(self):
        passwd = self.cfg['DATABASE']['Password']
        return self.cripto.decriptografar(passwd)


    def set_db_host(self, value: str):
        host = self.cripto.criptografar(value)
        try:
            self.cfg['DATABASE']['Host'] = host
        except KeyError:
            self.cfg.add_section('DATABASE')
            self.cfg.set('DATABASE', 'Host', host)


    def get_db_host(self):
        host = self.cfg['DATABASE']['Host']
        return self.cripto.decriptografar(host)


    def set_db_name(self, value: str):
        name = self.cripto.criptografar(value)
        try:
            self.cfg['DATABASE']['Name'] = name
        except KeyError:
            self.cfg.add_section('DATABASE')
            self.cfg.set('DATABASE', 'Name', name)


    def get_db_name(self):
        name = self.cfg['DATABASE']['Name']
        return self.cripto.decriptografar(name)


    def set_db_port(self, value: int):
        port = self.cripto.criptografar(str(value))
        try:
            self.cfg['DATABASE']['Port'] = port
        except KeyError:
            self.cfg.add_section('DATABASE')
            self.cfg.set('DATABASE', 'PORT', port)


    def get_db_port(self):
        port = self.cfg['DATABASE']['Port']
        return int(self.cripto.decriptografar(port))



