from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from bs4 import BeautifulSoup
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import *
from selenium.webdriver.chrome.options import Options
from random import randint
from time import sleep
from model.EmpBr import EmpresaBr
from random import random
from navigate.SiteScrapper import SiteScrapper


class Navegador:

    def __init__(self):
        self.webElement = None
        self.url = 'https://www.google.com'
        self.options = Options()
        # self.options.headless = True
        self.driver = webdriver.Chrome(options=self.options)
        self.emprbr = EmpresaBr()
        self.listOfArgs = []
        self.listOfArgsNotUsed = {}
        for item in self.emprbr.__dict__:
            self.listOfArgs.append(item)
            self.listOfArgsNotUsed[item] = False
        self.switcher = {
            'cnpj.biz': 1,
            'cnpj.rocks': 1,
            'cnpjreceita': 1,
            'consultascnpj': 1,
            'empresascnpj': 1
        }
        self.siteScrapper = SiteScrapper()
        self.switcher2 = {
            'cnpj.biz': self.siteScrapper.cnpj_biz,
            'cnpj.rocks': self.siteScrapper.cnpj_rocks,
            'cnpjreceita': self.siteScrapper.cnpjreceita,
            'consultascnpj': self.siteScrapper.consultascnpj,
            'empresascnpj': self.siteScrapper.empresascnpj
        }

    def iniciarNavegador(self):
        self.driver.get(self.url)

    def tempoimplicitoDeEspera(self):
        self.driver.implicitly_wait(randint(30, 50) + random())

    def aguardar_elemento(self, id_elemento):
        WebDriverWait(self.driver, 5).until(
            EC.presence_of_element_located((By.ID, id_elemento))
        )

    def fechar_alertas(self):
        WebDriverWait(self.driver, 10).until(EC.alert_is_present())
        alerta = self.driver.switch_to.alert()
        alerta.accept()

    def fecharAba(self):
        self.tempoExplicitoDeEspera()
        self.driver.close()

    def fecharNavegador(self):
        self.driver.quit()

    # def time_out(self, motivo):
    #     self.finaliza_processo()
    #     raise TimeoutException(motivo)
    # this is being done in another part,but can be utilized here
    def pesquisaGoogle(self, cnpj):
        self.procurar = self.driver.find_element_by_css_selector('.gLFyf.gsfi')  # campo de pesquisa do google
        self.procurar.clear()
        self.tempoExplicitoDeEspera()
        for character in cnpj:
            self.procurar.send_keys(character)
            sleep(random())  # pause for random time between 0 and 1
        self.tempoExplicitoDeEspera()
        self.procurar.submit()
        self.tempoExplicitoDeEspera()

    def getLinksFromGoogle(self):
        self.soup_level1 = BeautifulSoup(self.driver.page_source, 'lxml')
        for possibleDatas in self.soup_level1:
            if "1 resultado" in possibleDatas.text:
                self.findElements = self.driver.find_elements(By.XPATH,
                                                              "//*[@id='rso']/div/div/div/div/div[1]/a")
            else:
                self.findElements = self.driver.find_elements(By.XPATH,
                                                              "//*[@id='rso']/div/div/div/div/div/div/a")  # pega
                # todos os links
        return self.findElements

    def abrirLinkEmOutraAba(self, urlOfLink):
        func = 1
        func = self.switcher.get(urlOfLink, 0)
        if func:
            self.webElement.send_keys(Keys.CONTROL + Keys.RETURN)
            return func
        else:
            return func

    @staticmethod
    def tempoExplicitoDeEspera():
        sleep(randint(1, 3) + random())  # random pause between 1 and 4

    def SwitchToWindowsZero(self):
        self.driver.switch_to.window(self.windows[0])

    def SwitchToWindowsOne(self):
        self.driver.switch_to.window(self.windows[1])

    def WindowsHandling(self):
        self.windows = self.driver.window_handles

    @staticmethod
    def InvalidSite(cnpj, i):
        return 0

    def callwhichSiteScrapper(self, emprbr, socio, cnae, urlOflink):
        soup = BeautifulSoup(self.driver.page_source, 'lxml')
        self.switcher2.get(urlOflink)(emprbr, socio, cnae, soup)
