import re

from bs4 import BeautifulSoup

from model.EmpBr import EmpresaBr
from model.CnpjBiz import CnpjBiz
from model.empresasCnpj import EmpresasCnpj
from model.cnpjReceita import CnpjReceita
from model.cnpjRocks import CnpjRocks
from model.consultasCnpj import ConsultasCnpj
from Converter.Conversor import Conversor


class SiteScrapper:
    def __init__(self):

        self.cnpjBiz = CnpjBiz()
        self.empresasCnpj = EmpresasCnpj()
        self.cnpjReceita = CnpjReceita()
        self.cnpjRocks = CnpjRocks()
        self.consultasCnpj = ConsultasCnpj()
        self.conversor = Conversor()

    def cnpj_biz(self, emprbr, socio, cnae, soup):
        listOfArgsEmps = []
        listOfArgsCnaes = []
        listOfArgsSocio = []
        listOfArgsNotUsed = {}
        # for atributo in self.cnpjBiz.__getattribute__('EmpresaBr'):
        #     for item in atributo:
        #         listOfArgs.append(item)
        for item in self.cnpjBiz.__getattribute__('EmpresaBr'):
            listOfArgsEmps.append(item)
        Ptags = soup.find_all('p')
        cnaes = soup.find_all('b')
        socios = soup.find('h2', text='Quadro de Sócios e Administradores')
        if Ptags:
            for tag in Ptags:
                for arg in listOfArgsEmps:
                    stringAfterSplit = None
                    if bool(re.search(arg, tag.text, re.IGNORECASE)):
                        if re.search(r'[:]', tag.text):
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        else:
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        self.cnpjBiz.__dict__['EmpresaBr'][arg] = stringAfterSplit
                        break
                    else:
                        continue
            for item in self.cnpjBiz.__getattribute__('Cnaes'):
                listOfArgsCnaes.append(item)
            self.cnpjBiz.__dict__['Cnaes']['CNPJ'] = self.cnpjBiz.__dict__['EmpresaBr']['CNPJ']
        counter = 1
        if cnaes:
            for tag in cnaes:
                stringAfterSplit = None
                if bool(re.search(r'[0-9]{2}\.[0-9]{2}-[0-9]-[0-9]{2}', tag.text, re.IGNORECASE)):
                    if re.search(r'[:]', tag.text):
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    else:
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    if counter == 2:
                        self.cnpjBiz.__dict__['Cnaes']['Secundária(s)'] = stringAfterSplit
                    elif counter == 1:
                        self.cnpjBiz.__dict__['Cnaes']['Principal'] = stringAfterSplit
                    counter = 2
                    break
                else:
                    continue
        for item in self.cnpjBiz.__getattribute__('Sócios'):
            listOfArgsSocio.append(item)
        self.cnpjBiz.__dict__['Sócios']['CNPJ'] = self.cnpjBiz.__dict__['EmpresaBr']['CNPJ']
        if socios:
            contador = 0
            for tag in socios.next_sibling.next_sibling:
                stringAfterSplit1 = re.split(r'(\w+?\s)+- ', tag)[-1]
                stringAfterSplit2 = re.split(r' - .+', tag)[-1]
                self.cnpjBiz.__dict__['Sócios']['tipo'][contador] = stringAfterSplit1
                self.cnpjBiz.__dict__['Sócios']['nome'][contador] = stringAfterSplit2
                contador += 1
        self.conversor.convertCnpjBiz(emprbr, socio, cnae, self.cnpjBiz)

    def cnpj_rocks(self, emprbr, socio, cnae, soup):
        listOfArgsEmps = []
        listOfArgsCnaes = []
        listOfArgsSocio = []
        listOfArgsNotUsed = {}
        # for atributo in self.cnpjBiz.__getattribute__('EmpresaBr'):
        #     for item in atributo:
        #         listOfArgs.append(item)
        for item in self.cnpjRocks.__getattribute__('EmpresaBr'):
            listOfArgsEmps.append(item)
        Ptags = soup.find_all('p')
        cnaes = soup.find_all('b')
        socios = soup.find('h2', text='Quadro de Sócios e Administradores')
        if Ptags:
            for tag in Ptags:
                for arg in listOfArgsEmps:
                    stringAfterSplit = None
                    if bool(re.search(arg, tag.text, re.IGNORECASE)):
                        if re.search(r'[:]', tag.text):
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        else:
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        self.cnpjRocks.__dict__['EmpresaBr'][arg] = stringAfterSplit
                        break
                    else:
                        continue
            for item in self.cnpjRocks.__getattribute__('Cnaes'):
                listOfArgsCnaes.append(item)
            self.cnpjRocks.__dict__['Cnaes']['CNPJ'] = self.cnpjRocks.__dict__['EmpresaBr']['CNPJ']
        counter = 1
        if cnaes:
            for tag in cnaes:
                stringAfterSplit = None
                if bool(re.search(r'[0-9]{2}\.[0-9]{2}-[0-9]-[0-9]{2}', tag.text, re.IGNORECASE)):
                    if re.search(r'[:]', tag.text):
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    else:
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    if counter == 2:
                        self.cnpjRocks.__dict__['Cnaes']['Secundária(s)'] = stringAfterSplit
                    elif counter == 1:
                        self.cnpjRocks.__dict__['Cnaes']['Principal'] = stringAfterSplit
                    counter = 2
                    break
                else:
                    continue
        for item in self.cnpjRocks.__getattribute__('Sócios'):
            listOfArgsSocio.append(item)
        self.cnpjRocks.__dict__['Sócios']['CNPJ'] = self.cnpjRocks.__dict__['EmpresaBr']['CNPJ']
        if socios:
            contador = 0
            for tag in socios.next_sibling.next_sibling:
                stringAfterSplit1 = re.split(r'(\w+?\s)+- ', tag)[-1]
                stringAfterSplit2 = re.split(r' - .+', tag)[-1]
                self.cnpjRocks.__dict__['Sócios']['tipo'][contador] = stringAfterSplit1
                self.cnpjRocks.__dict__['Sócios']['nome'][contador] = stringAfterSplit2
                contador += 1
        self.conversor.convertCnpjRocks(emprbr, socio, cnae, self.cnpjRocks)

    def cnpjreceita(self, emprbr, socio, cnae, soup):
        listOfArgsEmps = []
        listOfArgsCnaes = []
        listOfArgsSocio = []
        listOfArgsNotUsed = {}
        # for atributo in self.cnpjBiz.__getattribute__('EmpresaBr'):
        #     for item in atributo:
        #         listOfArgs.append(item)
        for item in self.cnpjReceita.__getattribute__('EmpresaBr'):
            listOfArgsEmps.append(item)
        Ptags = soup.find_all('p')
        cnaes = soup.find_all('b')
        socios = soup.find('h2', text='Quadro de Sócios e Administradores')
        if Ptags:
            for tag in Ptags:
                for arg in listOfArgsEmps:
                    stringAfterSplit = None
                    if bool(re.search(arg, tag.text, re.IGNORECASE)):
                        if re.search(r'[:]', tag.text):
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        else:
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        self.cnpjReceita.__dict__['EmpresaBr'][arg] = stringAfterSplit
                        break
                    else:
                        continue
            for item in self.cnpjReceita.__getattribute__('Cnaes'):
                listOfArgsCnaes.append(item)
            self.cnpjReceita.__dict__['Cnaes']['CNPJ'] = self.cnpjReceita.__dict__['EmpresaBr']['CNPJ']
        counter = 1
        if cnaes:
            for tag in cnaes:
                stringAfterSplit = None
                if bool(re.search(r'[0-9]{2}\.[0-9]{2}-[0-9]-[0-9]{2}', tag.text, re.IGNORECASE)):
                    if re.search(r'[:]', tag.text):
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    else:
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    if counter == 2:
                        self.cnpjReceita.__dict__['Cnaes']['Secundária(s)'] = stringAfterSplit
                    elif counter == 1:
                        self.cnpjReceita.__dict__['Cnaes']['Principal'] = stringAfterSplit
                    counter = 2
                    break
                else:
                    continue
        for item in self.cnpjReceita.__getattribute__('Sócios'):
            listOfArgsSocio.append(item)
        self.cnpjReceita.__dict__['Sócios']['CNPJ'] = self.cnpjReceita.__dict__['EmpresaBr']['CNPJ']
        if socios:
            contador = 0
            for tag in socios.next_sibling.next_sibling:
                stringAfterSplit1 = re.split(r'(\w+?\s)+- ', tag)[-1]
                stringAfterSplit2 = re.split(r' - .+', tag)[-1]
                self.cnpjReceita.__dict__['Sócios']['tipo'][contador] = stringAfterSplit1
                self.cnpjReceita.__dict__['Sócios']['nome'][contador] = stringAfterSplit2
                contador += 1
        self.conversor.convertCnpjReceitas(emprbr, socio, cnae, self.cnpjReceita)

    def consultascnpj(self, emprbr, socio, cnae, soup):
        listOfArgsEmps = []
        listOfArgsCnaes = []
        listOfArgsSocio = []
        listOfArgsNotUsed = {}
        # for atributo in self.cnpjBiz.__getattribute__('EmpresaBr'):
        #     for item in atributo:
        #         listOfArgs.append(item)
        for item in self.consultasCnpj.__getattribute__('EmpresaBr'):
            listOfArgsEmps.append(item)
        Ptags = soup.find_all('p')
        cnaes = soup.find_all('b')
        socios = soup.find('h2', text='Quadro de Sócios e Administradores')
        if Ptags:
            for tag in Ptags:
                for arg in listOfArgsEmps:
                    stringAfterSplit = None
                    if bool(re.search(arg, tag.text, re.IGNORECASE)):
                        if re.search(r'[:]', tag.text):
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        else:
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        self.consultasCnpj.__dict__['EmpresaBr'][arg] = stringAfterSplit
                        break
                    else:
                        continue
            for item in self.consultasCnpj.__getattribute__('Cnaes'):
                listOfArgsCnaes.append(item)
            self.consultasCnpj.__dict__['Cnaes']['CNPJ'] = self.consultasCnpj.__dict__['EmpresaBr']['CNPJ']
        counter = 1
        if cnaes:
            for tag in cnaes:
                stringAfterSplit = None
                if bool(re.search(r'[0-9]{2}\.[0-9]{2}-[0-9]-[0-9]{2}', tag.text, re.IGNORECASE)):
                    if re.search(r'[:]', tag.text):
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    else:
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    if counter == 2:
                        self.consultasCnpj.__dict__['Cnaes']['Secundária(s)'] = stringAfterSplit
                    elif counter == 1:
                        self.consultasCnpj.__dict__['Cnaes']['Principal'] = stringAfterSplit
                    counter = 2
                    break
                else:
                    continue
        for item in self.consultasCnpj.__getattribute__('Sócios'):
            listOfArgsSocio.append(item)
        self.consultasCnpj.__dict__['Sócios']['CNPJ'] = self.consultasCnpj.__dict__['EmpresaBr']['CNPJ']
        if socios:
            contador = 0
            for tag in socios.next_sibling.next_sibling:
                stringAfterSplit1 = re.split(r'(\w+?\s)+- ', tag)[-1]
                stringAfterSplit2 = re.split(r' - .+', tag)[-1]
                self.consultasCnpj.__dict__['Sócios']['tipo'][contador] = stringAfterSplit1
                self.consultasCnpj.__dict__['Sócios']['nome'][contador] = stringAfterSplit2
                contador += 1
        self.conversor.convertConsultasCnpj(emprbr, socio, cnae, self.consultasCnpj)

    def empresascnpj(self, emprbr, socio, cnae, soup):
        listOfArgsEmps = []
        listOfArgsCnaes = []
        listOfArgsSocio = []
        listOfArgsNotUsed = {}
        # for atributo in self.cnpjBiz.__getattribute__('EmpresaBr'):
        #     for item in atributo:
        #         listOfArgs.append(item)
        for item in self.empresasCnpj.__getattribute__('EmpresaBr'):
            listOfArgsEmps.append(item)
        Ptags = soup.find_all('p')
        cnaes = soup.find_all('b')
        socios = soup.find('h2', text='Quadro de Sócios e Administradores')
        if Ptags:
            for tag in Ptags:
                for arg in listOfArgsEmps:
                    stringAfterSplit = None
                    if bool(re.search(arg, tag.text, re.IGNORECASE)):
                        if re.search(r'[:]', tag.text):
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        else:
                            stringAfterSplit = re.split('\\b' + arg + '\\b', tag.text)[-1]
                        self.empresasCnpj.__dict__['EmpresaBr'][arg] = stringAfterSplit
                        break
                    else:
                        continue
            for item in self.empresasCnpj.__getattribute__('Cnaes'):
                listOfArgsCnaes.append(item)
            self.empresasCnpj.__dict__['Cnaes']['CNPJ'] = self.empresasCnpj.__dict__['EmpresaBr']['CNPJ']
        counter = 1
        if cnaes:
            for tag in cnaes:
                stringAfterSplit = None
                if bool(re.search(r'[0-9]{2}\.[0-9]{2}-[0-9]-[0-9]{2}', tag.text, re.IGNORECASE)):
                    if re.search(r'[:]', tag.text):
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    else:
                        stringAfterSplit = re.split(r'- ', tag.text)[-2]
                    if counter == 2:
                        self.empresasCnpj.__dict__['Cnaes']['Secundária(s)'] = stringAfterSplit
                    elif counter == 1:
                        self.empresasCnpj.__dict__['Cnaes']['Principal'] = stringAfterSplit
                    counter = 2
                    break
                else:
                    continue
        for item in self.empresasCnpj.__getattribute__('Sócios'):
            listOfArgsSocio.append(item)
        self.empresasCnpj.__dict__['Sócios']['CNPJ'] = self.empresasCnpj.__dict__['EmpresaBr']['CNPJ']
        if socios:
            contador = 0
            for tag in socios.next_sibling.next_sibling:
                stringAfterSplit1 = re.split(r'(\w+?\s)+- ', tag)[-1]
                stringAfterSplit2 = re.split(r' - .+', tag)[-1]
                self.empresasCnpj.__dict__['Sócios']['tipo'][contador] = stringAfterSplit1
                self.empresasCnpj.__dict__['Sócios']['nome'][contador] = stringAfterSplit2
                contador += 1
        self.conversor.convertEmpresasCnpj(emprbr, socio, cnae, self.empresasCnpj)

# class CnpjBizSiteScrapper(SiteScrapper):
#     def __init__(self):
#         super().__init__()
#         self.emprbrCnpjBiz = [EmpresaBr(cnpjbiz='cnpjbiz') for i in range(600)]
#
#
# class CnpjRocksSiteScrapper(SiteScrapper):
#     def __init__(self):
#         super().__init__()
#         self.emprbrCnpjRocks = [EmpresaBr(cnpjrocks='cnpjrocks') for i in range(600)]
#
#
# class ConsultasCnpjSiteScrapper(SiteScrapper):
#     def __init__(self):
#         super().__init__()
#         self.emprbrConsultasCnpj = [EmpresaBr(cnpjbiz='consultascnpj') for i in range(600)]
#
#
# class CnpjReceitaSiteScrapper(SiteScrapper):
#     def __init__(self):
#         super().__init__()
#         self.emprbrCnpjReceita = [EmpresaBr(cnpjbiz='cnpjreceita') for i in range(600)]
#
#
# class EmpresasCnpjSiteScrapper(SiteScrapper):
#     def __init__(self):
#         super().__init__()
#         self.emprbrEmpresasCnpj = [EmpresaBr(cnpjbiz='empresascnpj') for i in range(600)]
