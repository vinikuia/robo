import re


class Conversor:
    def __init__(self):
        self.something = None

    @staticmethod
    def convertCnpjBiz(empresaBr, socio, cnae, cnpjBiz):
        if cnpjBiz:
            for atributo in empresaBr.__dict__:
                for item in cnpjBiz.__dict__['EmpresaBr']:
                    if empresaBr.__dict__[atributo]['cnpjbiz']:
                        empresaBr.__dict__[atributo]['cnpjbiz'] = cnpjBiz.__dict__['EmpresaBr'][
                            empresaBr.__dict__[atributo]['cnpjbiz']]
                        break
            for atributo in socio.__dict__:
                for item in cnpjBiz.__dict__['Sócio']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjBiz.__dict__['Sócio'][
                            empresaBr.__dict__[atributo]['cnpjRocks']]

            for atributo in cnae.__dict__:
                for item in cnpjBiz.__dict__['Cnaes']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjBiz.__dict__['Cnaes'][
                            empresaBr.__dict__[atributo][
                                'cnpjRocks']]

    @staticmethod
    def convertCnpjRocks(empresaBr, socio, cnae, cnpjRocks):
        if cnpjRocks:
            for atributo in empresaBr.__dict__:
                for item in cnpjRocks.__dict__['EmpresaBr']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        if empresaBr.__dict__[atributo]['cnpjRocks'] == 'Telefone':
                            empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjRocks.__dict__['EmpresaBr'][
                                empresaBr.__dict__[atributo]['cnpjRocks']]
                        break
            for atributo in socio.__dict__:
                for item in cnpjRocks.__dict__['Sócio']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjRocks.__dict__['Sócio'][
                            empresaBr.__dict__[atributo]['cnpjRocks']]

            for atributo in cnae.__dict__:
                for item in cnpjRocks.__dict__['Cnaes']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjRocks.__dict__['Cnaes'][
                            empresaBr.__dict__[atributo][
                                'cnpjRocks']]

    @staticmethod
    def convertCnpjReceitas(empresaBr, socio, cnae, cnpjReceita):
        if cnpjReceita:
            for atributo in empresaBr.__dict__:
                for item in cnpjReceita.__dict__['EmpresaBr']:
                    if empresaBr.__dict__[atributo]['cnpjReceita']:
                        empresaBr.__dict__[atributo]['cnpjReceita'] = cnpjReceita.__dict__['EmpresaBr'][
                            empresaBr.__dict__[atributo]['cnpjReceita']]
                        break
            for atributo in socio.__dict__:
                for item in cnpjReceita.__dict__['Sócio']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjReceita.__dict__['Sócio'][
                            empresaBr.__dict__[atributo]['cnpjRocks']]

            for atributo in cnae.__dict__:
                for item in cnpjReceita.__dict__['Cnaes']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = cnpjReceita.__dict__['Cnaes'][
                            empresaBr.__dict__[atributo][
                                'cnpjRocks']]

    @staticmethod
    def convertConsultasCnpj(empresaBr, socio, cnae, consultasCnpj):
        if consultasCnpj:
            for atributo in empresaBr.__dict__:
                for item in consultasCnpj.__dict__['EmpresaBr']:
                    if empresaBr.__dict__[atributo]['consultasCnpj']:
                        empresaBr.__dict__[atributo]['consultasCnpj'] = consultasCnpj.__dict__['EmpresaBr'][
                            empresaBr.__dict__[atributo]['consultasCnpj']]
                        break
            for atributo in socio.__dict__:
                for item in consultasCnpj.__dict__['Sócio']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = consultasCnpj.__dict__['Sócio'][
                            empresaBr.__dict__[atributo]['cnpjRocks']]

            for atributo in cnae.__dict__:
                for item in consultasCnpj.__dict__['Cnaes']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = consultasCnpj.__dict__['Cnaes'][
                            empresaBr.__dict__[atributo][
                                'cnpjRocks']]

    @staticmethod
    def convertEmpresasCnpj(empresaBr, socio, cnae, empresasCnpj):
        if empresasCnpj:
            for atributo in empresaBr.__dict__:
                for item in empresasCnpj.__dict__['EmpresaBr']:
                    if empresaBr.__dict__[atributo]['empresasCnpj']:
                        empresaBr.__dict__[atributo]['empresasCnpj'] = empresasCnpj.__dict__['EmpresaBr'][
                            empresaBr.__dict__[atributo]['empresasCnpj']]
                        break
            for atributo in socio.__dict__:
                for item in empresasCnpj.__dict__['Sócio']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = empresasCnpj.__dict__['Sócio'][
                            empresaBr.__dict__[atributo]['cnpjRocks']]

            for atributo in cnae.__dict__:
                for item in empresasCnpj.__dict__['Cnaes']:
                    if empresaBr.__dict__[atributo]['cnpjRocks']:
                        empresaBr.__dict__[atributo]['cnpjRocks'].append = empresasCnpj.__dict__['Cnaes'][
                            empresaBr.__dict__[atributo][
                                'cnpjRocks']]

    def DecideWhich(self,empresaBr, socio, cnae, empresasCnpj):
        

            #
            #
            #
            # for item in cnpjBiz:
            #     empresaBr['cnpj'] = cnpjBiz['empresaBr']['CNPJ']
            #     empresaBr['nome_empresarial'] = cnpjBiz['empresaBr']['Razão Social']
            #     empresaBr['data_inicio_atividade'] = cnpjBiz['empresaBr']['Data da Abertura']
            #     empresaBr['id_matriz_filial'] = cnpjBiz['empresaBr']['Tipo']
            #     empresaBr['nome_fantasia'] = cnpjBiz['empresaBr']['Nome Fantasia']
            #     empresaBr['situacao_cadastral'] = cnpjBiz['empresaBr']['Situação']
            #     empresaBr['codigo_natureza_juridica'] = cnpjBiz['empresaBr']['Natureza Jurídica']
            #     empresaBr['endereco_e_mail'] = cnpjBiz['empresaBr']['E-mail']
            #     empresaBr['telefone1'] = cnpjBiz['empresaBr']['Telefone']
            #     empresaBr['logradouro'] = cnpjBiz['empresaBr']['Logradouro']
            #     empresaBr['complemento'] = cnpjBiz['empresaBr']['Complemento']
            #     empresaBr['bairro'] = cnpjBiz['empresaBr']['Bairro']
            #     empresaBr['cep'] = cnpjBiz['empresaBr']['CEP']
            #     empresaBr['municipio'] = cnpjBiz['empresaBr']['Município']
            #     empresaBr['uf'] = cnpjBiz['empresaBr']['Estado']
