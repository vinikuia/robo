from connection.connect import DBManager
from controle.controle import ControleDoNavegador
from cripto.KeyWork import KeyWork
from cripto.crypto import Cripto
import schedule
import time
import datetime
import threading


class Main():
    def __init__(self):
        self.hasto = 1
        self.file = open('./cripto/key.key', 'rb')
        self.key = self.file.read()
        self.arquivo = './cripto/Nsinova.cfg'
        self.cripto = Cripto(key=self.key)
        self.keyWork = KeyWork(chave=self.key, arquivo=self.arquivo)
        parametros_db = {'user': self.keyWork.get_db_user(),
                         'password': self.keyWork.get_db_passwd(),
                         'host': self.keyWork.get_db_host(),
                         'port': self.keyWork.get_db_port(),
                         'database': self.keyWork.get_db_name()}
        self.conn = DBManager(**parametros_db)

    def main(self):
        try:
            self.conn.abrir_con()
            while self.hasto:
                self.texto = self.cripto.criptografar(
                    "select cnpj from empresas.empresa_brasileira WHERE situacao_cadastral = 2 limit 600")
                self.cnpjs = self.conn.executar_select(self.cripto.decriptografar(self.texto))
                self.controle = ControleDoNavegador()
                self.controle.navigateThroughGoogleAndOpenLinks(self)
        finally:
            self.conn.fechar_con()
            self.hasto = 1

    def run_threaded(self, job_func):
        self.job_thread = threading.Thread(target=job_func)
        self.job_thread.start()

    def has_to_stop(self):
        self.hasto = 0


if __name__ == '__main__':
    main = Main()
    now = datetime.datetime.now().time()
    minute = now.minute + 10
    hour = now.hour
    if 0 <= now.hour <= 9 and 0 <= now.minute <= 8:
        schedule.every().day.at('0' + str(now.hour) + ":" + '0' + str(now.minute + 1)).do(main.run_threaded,
                                                                                          main.main).tag(
            "breakfast main")
    elif 0 <= now.minute <= 8:
        schedule.every().day.at(str(now.hour) + ":" + '0' + str(now.minute + 1)).do(main.run_threaded, main.main).tag(
            "breakfast main")
    elif 0 <= now.hour <= 9:
        schedule.every().day.at('0' + str(now.hour) + ":" + str(now.minute + 1)).do(main.run_threaded, main.main).tag(
            "breakfast main")
    elif now.hour == 12 and 0 <= now.minute <= 8:
        schedule.every().day.at('12' ":" + '0' + str(now.minute + 1)).do(main.run_threaded, main.main).tag(
            "breakfast main")
    elif now.hour == 12:
        schedule.every().day.at('12' ":" + str(now.minute + 1)).do(main.run_threaded, main.main).tag(
            "breakfast main")
    else:
        schedule.every().day.at(str(now.hour) + ":" + str(now.minute+1)).do(main.run_threaded, main.main).tag(
            "breakfast main")  # if minutes are from 0 to nine then it gives an error
    schedule.every().day.at("13:00").do(main.run_threaded, main.main).tag("launch main")
    while True:
        schedule.run_pending()
        now = datetime.datetime.now().time()
        print(now)
        if now.hour == hour and now.minute == minute:
            print("cancelado ao meio dia o scrapping da manhã")
            schedule.clear('breakfast main')
            main.has_to_stop()
        if now.hour == 18 and now.minute == 0:
            print("cancelado as 18h o scrapping da tarde")
            schedule.clear('launch main')
        time.sleep(30)

    # to make it periodic
    # scheduler = BlockingScheduler()
    # scheduler.add_executor('processpool')
    # scheduler.add_job(main, 'interval')
    # scheduler.start()

    # main()
    # exit(0)
    # make it run one time
