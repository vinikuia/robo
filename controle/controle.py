from navigate.Navegador import Navegador
from selenium.common.exceptions import TimeoutException
from urllib.parse import urlparse
from model.EmpBr import EmpresaBr
from model.socio import Socio
from model.Cnaes import Cnae


class ControleDoNavegador:
    def __init__(self):

        self.navegador = Navegador()
        self.navegador.iniciarNavegador()
        self.emprbr = [EmpresaBr() for i in range(600)]
        self.cnae = [Cnae() for i in range(600)]
        self.socio = [Socio() for i in range(600)]
        self.switcher = {
            'cnpj.biz': self.navegador.callwhichSiteScrapper,
            'cnpj.rocks': self.navegador.callwhichSiteScrapper,
            'cnpjreceita': self.navegador.callwhichSiteScrapper,
            'consultascnpj': self.navegador.callwhichSiteScrapper,
            'empresascnpj': self.navegador.callwhichSiteScrapper
        }

    def navigateThroughGoogleAndOpenLinks(self, main):
        i = 0
        try:
            for cnpj in main.cnpjs:
                if not getattr(main, 'hasto'):
                    break
                self.navegador.tempoimplicitoDeEspera()
                self.navegador.element = None
                self.navegador.findElements = None
                self.navegador.pesquisaGoogle(cnpj[0])
                try:
                    self.navegador.aguardar_elemento("resultStats")
                except TimeoutException:
                    continue
                self.navegador.findElements = self.navegador.getLinksFromGoogle()
                for self.navegador.webElement in self.navegador.findElements:
                    if not getattr(main, 'hasto'):
                        break
                    urlOflink = urlparse(self.navegador.webElement.get_attribute("Href")).hostname
                    self.navegador.tempoExplicitoDeEspera()
                    continueOrNOt = self.navegador.abrirLinkEmOutraAba(urlOflink)
                    if not continueOrNOt:
                        continue
                    self.navegador.WindowsHandling()
                    self.navegador.SwitchToWindowsOne()
                    self.switcher.get(urlOflink)(self.emprbr[i], self.socio[i], self.cnae[i], urlOflink)
                    self.navegador.fecharAba()
                    self.navegador.SwitchToWindowsZero()
                i += 1
        finally:
            self.navegador.fecharNavegador()

    # def carregarCFG(self) -> Config:
    #     chave = open('./chave.key').readline()
    #     return Config(chave=chave, arquivo='./ns2e.cfg')
    # 
    # def carregarBase(self) -> DBManager:
    #     """
    #     instancia um objeto DBManager com as configurações presentes no atributo cfg
    #     :return: Objeto apra comunicação com a base de dados.
    #     """
    #     parametros_db = {'user': self.cfg.get_db_user(),
    #                      'password': self.cfg.get_db_passwd(),
    #                      'host': self.cfg.get_db_url(),
    #                      'port': self.cfg.get_db_port(),
    #                      'database': self.cfg.get_db_name()}
    #     return DBManager(**parametros_db)
